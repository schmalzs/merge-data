# Technical Exercise

## Task

There exists a User API with an endpoint to generate a set of data that will be used when obtaining insurance quotes for a user. The data generated is pulled from existing and/or previous insurance policies that have been associated with the user.

Currently, this endpoint does not work as desired. Your job is to fix it.

When generating the data, the following rules apply:

1. Data from the user's existing policies should be blended together to provide the most holistic view of the user.
1. Each policy may not be fully complete. There may be information that is missing.
1. When two or more policies exist with conflicting information, the policy with the most recent issue date should generally be used. Assume that the issue date is unique for each policy.
1. Only the operators from the most recent policy should be included in the data. Assume that each operator can be uniquely identified by their `relationship` field. Assume that two operators will never have the same `relationship` value. The same operator may exist on multiple policies and their data may be blended together.
1. Addresses should never be blended together. Addresses frequently change as people move. Blending them may accidentally result in a mixture of two completely different addresses. The newest address should be used in its entirety.

## Prerequisites

- Node v14+
- Install dependencies: `npm install`

## NPM Scripts

| Script | Description                                                               |
| ------ | ------------------------------------------------------------------------- |
| build  | Build the app.                                                            |
| dev    | Run the app in development mode. The app will be re-built on each change. |
| docs   | Generate the documentation.                                               |
| lint   | Lint the code.                                                            |
| start  | Start the app. Note that the app must be built first.                     |
| test   | Run the tests.                                                            |

## API Documentation

Once you start the server, you may access the API docs for the endpoint at [http://localhost:3000/docs](http://localhost:3000/docs).
