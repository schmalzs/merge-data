import combineData from '../combine-data';

import type { Policy } from '../../types';

describe('combine-data', () => {
  let policies: Policy[];

  beforeEach(() => {
    policies = [
      {
        policyId: '2c2d1a2f-16cd-4c59-bab0-95cc1c30c02b',
        issuer: 'libertymutual',
        issueDate: '2021-11-30',
        renewalDate: '2022-05-31',
        policyTermMonths: 6,
        premiumCents: 73443,
        policyholder: {
          name: {
            firstName: 'Sophia',
            middleName: '',
            lastName: 'King',
          },
          address: {
            city: 'Pleasantville',
            state: 'WY',
            zip: '32555',
            plus4: '1234',
          },
          email: 'Sophia.King942@gmail.com',
        },
        operators: [
          {
            isPrimary: true,
            birthdayRange: {
              start: '1981-05-04',
              end: '1981-05-04',
            },
            gender: 'female',
            driversLicenseState: 'WY',
            driversLicenseStatus: 'ValidUSLicense',
            driversLicenseNumber: 'DL770078',
            relationship: 'Named Insured',
          },
          {
            isPrimary: false,
            name: {
              firstName: 'Joshua',
              middleName: 'Ryan',
              lastName: 'King',
            },
            birthdayRange: {
              start: '1986-06-19',
              end: '1986-06-19',
            },
            gender: 'male',
            driversLicenseState: 'WY',
            driversLicenseStatus: 'ValidUSLicense',
            driversLicenseNumber: 'DL225014',
            relationship: 'Spouse',
          },
        ],
      },
      {
        policyId: '7e58d267-df19-491a-94b3-0c6294d5dbf6',
        issuer: 'progressive',
        issueDate: '2020-11-30',
        renewalDate: '2021-05-31',
        policyTermMonths: 6,
        premiumCents: 84611,
        policyholder: {
          address: {
            city: 'Goyettebury',
            state: 'WY',
            zip: '32465',
            plus4: '0830',
          },
        },
        operators: [
          {
            isPrimary: true,
            name: {
              firstName: 'Sophia',
              middleName: '',
              lastName: 'King',
            },
            birthdayRange: {
              start: '1981-05-04',
              end: '1981-05-04',
            },
            gender: 'female',
            driversLicenseState: 'WY',
            driversLicenseStatus: 'ValidUSLicense',
            driversLicenseNumber: 'DL770078',
            relationship: 'Named Insured',
          },
          {
            isPrimary: false,
            name: {
              firstName: 'Joshua',
              middleName: '',
              lastName: 'King',
            },
            birthdayRange: {
              start: '1986-06-20',
              end: '1986-06-20',
            },
            gender: 'male',
            driversLicenseState: 'WY',
            driversLicenseStatus: 'ValidUSLicense',
            driversLicenseNumber: 'DL225013',
            relationship: 'Spouse',
          },
        ],
      },
    ];
  });

  it('should return return combined policy data', () => {
    expect(combineData(policies)).toEqual({
      policyholder: {
        name: {
          firstName: 'Sophia',
          middleName: 'Marie',
          lastName: 'King',
        },
        address: {
          city: 'Pleasantville',
          state: 'WY',
          zip: '32555',
          plus4: '1234',
        },
        email: 'Sophia.King942@gmail.com',
      },
      operators: [
        {
          isPrimary: true,
          name: {
            firstName: 'Sophia',
            middleName: 'Marie',
            lastName: 'King',
          },
          birthdayRange: {
            start: '1981-05-04',
            end: '1981-05-04',
          },
          gender: 'female',
          driversLicenseState: 'WY',
          driversLicenseStatus: 'ValidUSLicense',
          driversLicenseNumber: 'DL770078',
          relationship: 'Named Insured',
        },
        {
          isPrimary: false,
          name: {
            firstName: 'Joshua',
            middleName: 'Ryan',
            lastName: 'King',
          },
          birthdayRange: {
            start: '1986-06-19',
            end: '1986-06-19',
          },
          gender: 'male',
          driversLicenseState: 'WY',
          driversLicenseStatus: 'ValidUSLicense',
          driversLicenseNumber: 'DL225014',
          relationship: 'Spouse',
        },
      ],
    });
  });
});
