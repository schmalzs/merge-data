import type { Policy } from '../types';

export default (policies?: Policy[]) => {
  return policies.reduce((acc, policy) => ({ ...acc, ...policy }), {});
};
