import { Router } from 'express';
import * as db from '../db';
import combineData from './combine-data';

const router = Router();

router.get('/:userId/quote-data', async (req, res) => {
  const { userId } = req.params;
  const user = await db.getUser(userId);
  const policies = await db.getPolicies(user.policies);
  res.json(await combineData(policies));
});

export default router;
