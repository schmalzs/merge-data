import express from 'express';
import user from './user/routes';

const PORT = process.env.PORT || 3000;

const app = express();

app.use('/user', user);
app.use(express.static(`./public`));

app.listen(PORT, () => {
  console.info(`Server is listening on port ${PORT}`);
});
