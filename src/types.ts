export type Policy = {
  policyId: string;
  issuer: string;
  issueDate: string;
  renewalDate: string;
  policyTermMonths: number;
  premiumCents: number;
  policyholder?: Policyholder;
  operators?: Operator[];
};

export type Policyholder = {
  name?: Name;
  address?: Address;
  email?: string;
};

export type Operator = {
  isPrimary: boolean;
  name?: Name;
  birthdayRange?: BirthdayRange;
  gender?: string;
  driversLicenseState?: string;
  driversLicenseStatus?: string;
  driversLicenseNumber?: string;
  relationship?: string;
};

export type Name = {
  firstName?: string;
  middleName?: string;
  lastName?: string;
};

export type Address = {
  city?: string;
  state?: string;
  zip?: string;
  plus4?: string;
};

export type BirthdayRange = {
  start?: string;
  end?: string;
};
