import policies from '../data/policies.json';
import users from '../data/users.json';

import type { Policy } from './types';

export const getPolicies = async (policyIds: string[]): Promise<Policy[]> => {
  const result = policies.find((policy) => policyIds.includes(policy.policyId));
  return result ? [result] : [];
};

export const getUser = async (userId: string) =>
  users.find((user) => user.userId === userId);
